#Python SDK for the Isilon API
__Requires OneFS version 7.1+__

## Basic Usage
_Get a node on the ifs filesystem, then print it's type and content_  

    from pyisi import OneFS
    fs = OneFS(user='user',
                password='password',
                hostname='hostname')
                
    f = fs.node('/ifs/path/to/file')
    f.type()
    f.content()

## Filesystem Types
A node on the filesystem has one of two types:  

* Object (File)
* Container (Directory)


### FileSystemObject
The FileSystemObject is the representation of an Object, or File in the Isilon API.  

#### .content()
Return the full content of an Object, reading everything into memory.

#### .stream(f, chunk_size=1024)
Stream the contents of an Object into a file like object (f), using the specified chunk size  

    fs_node = fs.node('/ifs/path/to/file')
    
    with open('./file', 'w') as f:
        fs_node.stream(f)
        
### FileSystemContainer
The FileSystemContainer is the representation of a Container, or Directory in the Isilon API.

#### .children()
Return a generator of all direct children of the container.

#### .objects(recurse=False)
Return a generator of all objects as direct children of a container
Optionally, if recurse=True then recursively return all objects in all sub-children of the container

#### .containers(recurse=False)
Same a .objects() but for containers

### FileSystemNode
The base class for both FileSystemContainer and FileSystemObject.

#### .acl()

#### .metadata()
Return a dictionary of all metadata for the node.
Note that this dict may not always have the same keys, depending on the metadata available.

#### .size()
Get the size in bytes for the node

## Network Exports

### SMB
    OneFS.smb(share_name=None)

If share_name is none then return a list of all shares in the API. If specified then return a single share instance.
An AEC_NOT_FOUND is raised if a name is specified but not found.

### Quota
    OneFS.quota(qutoa_id=None)

Same as SMB but for quotas.

### NFS
    OneFS.nfs(export_id=None)

Same as SMB but for NFS exports.


## Exceptions
For every API request the response is evaluated and an exception may be raised:

* AEC_ERROR
    * Generic Error, all other AEC_* errors are inherited from this.
* AEC_NOT_FOUND
    * HTTP 404
* AEC_FORBIDDEN
    * HTTP 403
* AEC_UNAUTHORIZED
    * HTTP 401
* AEC_SYSTEM_INTERNAL_ERROR
    * HTTP 500
* AEC_CONNECTION_ERROR
    * Raised if we cannot make a connection to the API



